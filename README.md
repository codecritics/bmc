# Projet BMC

<p align="center"><img src="https://gitlab.com/codecritics/bmc/raw/master/frontend/src/assets/logo-bmc.png" /></p>

---

The Project BMC is an application that display the projects that I have been worked with.

I have implemented it using :

* NodeJs + ExpressJs + MongoDb + GraphQl

* Angular 5 + Bootstrap 4.

### Installing

A step by step guide to install.

Install the Backend dependencies and launch the server:

``` sh
cd backend && npm install && npm run dev
```

* Additional informations about the backend can be [here](https://gitlab.com/codecritics/bmc/tree/master/backend)
Install the Frontend dependencies and launch the server:

``` sh
cd frontend && npm install && npm run start
```

* Additional informations about the frontend can be [here](https://gitlab.com/codecritics/bmc/tree/master/frontend)

### TODO
* Create a nice Layout.
* Added all my projects in MongoDb.
* Setup a gitlab pipeline to generate pages.
* What about some tests...