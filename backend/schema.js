import {makeExecutableSchema} from 'graphql-tools';
import resolvers from './resolvers';

/**
 * First the  schemers?, second the query then the mutations
 * @type {string[]}
 */
const typeDefs = [`
    type Company {
        id: String
        name: String
        description: String
        activityArea: String
        url: String
        voteCount: Int
    }
    type Founder {
        id: String
        name: String
        citizenship: String
    }
    type Query {   
        allCompanies(searchTerm: String): [Company]
        company(id: String): Company
    }
    type Mutation {
        addCompany(name: String!, description: String!, activityArea: String!, url: String!): Company
        upvote(id: String!): Company
        downvote(id: String!): Company
    }

`];

const schema = makeExecutableSchema({typeDefs, resolvers})

export default schema;