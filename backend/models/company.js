import mongoose from 'mongoose'
import uuid from 'uuid'
const Schema = mongoose.Schema;

const companySchema = new Schema({
    id: {type: String, default: uuid.v1 },
    name: String,
    description: String,
    activityArea: String,
    url: String,
    voteCount: {type: Number, default: 0}
});

companySchema.index({'$**': 'text'});

const model = mongoose.model('company', companySchema);

export default model;