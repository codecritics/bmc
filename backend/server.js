import express from 'express';
import cors from 'cors'
import bodyParser from 'body-parser'
import {graphqlExpress, graphiqlExpress} from 'apollo-server-express'
import schema from './schema'
import mongoose from 'mongoose'

const app = express();

app.use(cors())
mongoose.connect('mongodb://localhost/graphqlserver');

const connection = mongoose.connection;

connection.once('open', () => {
    console.log('MongoDB database connection  established  sucessfully');
})

/**
 * First Endpoint
 */
app.use('/graphiql', graphiqlExpress({
    endpointURL: 'graphql'
}));

/**
 * Second Endpoint
 */
app.use('/graphql', bodyParser.json(), graphqlExpress({schema}));

app.listen(5000, () => console.log('Express Server running on port 5000'));