## Backend

##### Dependencies
I use [Babel](https://babeljs.io/) watch to compile the application:
* Babel Cli.
* Babel present env.
* Babel Watch.
The Backend is composed of nodejs+express/graphql+mongobd

The Graphql dependencies are:
* Graphql
* Graphql-tools
* Apollo-server-express

---
