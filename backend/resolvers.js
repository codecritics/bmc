import mongoose from 'mongoose'
import companyModel from './models/company'

/**
 * Hard coded datas @Todo I could use some fakerjs here... or mongo DB
 * @type {Array}
 */
var companiesData = [
    {
        id: '1',
        name: 'EDF',
        description: 'The First Electricy Energy Provider In Europe.',
        activityArea: 'energy',
        url: 'http://www.edf.fr',
        voteCount: 0
    },
    {
        id: '2',
        name: 'ELIOT',
        description: 'A Metallic Produce Manifacturer.',
        activityArea: 'industry',
        url: 'http://www.eliotmt.com',
        voteCount: 0
    },
    {
        id: '3',
        name: 'BidMotion',
        description: 'A Marketing Platform Helps Brands To Reach New Consumers On mobile With CPA-optimized Mobile Campaigns.',
        activityArea: 'advertising',
        url: 'http://www.bidmotion.com/',
        voteCount: 0
    }
];

const resolvers = {
    Query: {
        allCompanies: (root, {searchTerm}) => {
            // return companiesData;
            if (searchTerm !== '')
                return companyModel.find({$text: {$search: searchTerm}}).sort({voteCount: 'desc'})
            else
                return companyModel.find().sort({voteCount: 'desc'});
        },
        company: (root, {id}) => {
            // return companyModel.findOne({id: id});
            return companyModel.findOne({id: id});
        }
    },
    Mutation: {
        upvote: (root, {id}) => {
            /*
                const company = companiesData.filter(course => {
                    return course.id === id
                })[0];
                course.voteCount++;
                return course;
            */
            return companyModel.findOneAndUpdate({id: id}, {$inc: {'voteCount': 1}}, {returnNewDocument: true})
        },
        downvote: (root, {id}) => {
            /*   const company = companiesData.filter(course => {
                   return course.id === id
               })[0];
               course.voteCount--;
               return course;
            */
            return companyModel.findOneAndUpdate({id: id}, {$inc: {'voteCount': -1}}, {returnNewDocument: true})
        },
        addCompany: (root, {name, description, activityArea, url}) => {
            //   return null;
            const company = new companyModel({
                name: name,
                description: description,
                activityArea: activityArea,
                url: url
            });
            return company.save()
        }
    }
}

export default resolvers;