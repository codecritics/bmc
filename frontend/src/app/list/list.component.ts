import {Component, OnInit, Input, OnChanges} from '@angular/core';
import {Observable} from 'rxjs'

import {Company} from '../types'
import {CompanyService} from '../company.service'

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit, OnChanges {
  @Input() searchTerm: String;

  companies: Observable<Company[]>

  constructor(private companyservice: CompanyService) {
  }

  ngOnInit() {
    this.companies = this.companyservice.getAllCompanies(this.searchTerm)
  }

  ngOnChanges() {
    this.companies = this.companyservice.getAllCompanies(this.searchTerm)
  }
}
