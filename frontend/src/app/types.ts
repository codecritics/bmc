export type Company = {
  id: string;
  name: string;
  description: string;
  url: string;
  activityArea: string;
  voteCount: number;
}

export type Query = {
  allCompanies: Company[];
}
