import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {Apollo, ApolloModule} from 'apollo-angular';
import {HttpLinkModule, HttpLink} from 'apollo-angular-link-http';
import {HttpClientModule} from '@angular/common/http'

import {AppComponent} from './app.component';
import {InMemoryCache} from "apollo-cache-inmemory";
import { ListComponent } from './list/list.component';
import { ItemComponent } from './item/item.component';
import {CompanyService} from './company.service'

@NgModule({
  declarations: [
    AppComponent,
    ListComponent,
    ItemComponent
  ],
  imports: [
    BrowserModule,
    ApolloModule,
    HttpLinkModule,
    HttpClientModule
  ],
  providers: [CompanyService],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(apollo: Apollo, httpLink: HttpLink) {
    apollo.create({
      link: httpLink.create({uri: 'http://localhost:5000/graphql'}),
      cache: new InMemoryCache()
    })
  }
}
