import {Injectable} from '@angular/core';
import {Apollo} from 'apollo-angular'
import gql from 'graphql-tag'
import {map, filter} from 'rxjs/operators'

import {Query, Company} from './types'

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  constructor(private apollo: Apollo) {
  }

  getAllCompanies(searchTerm: String) {
    return this.apollo.watchQuery<Query>({
      pollInterval: 500,
      query: gql `
        query allCompanies($searchTerm: String) {
          allCompanies(searchTerm: $searchTerm) {
            id
            name
            description
            url
            activityArea
            voteCount
           }
        } 
      `,
      variables: {
        searchTerm: searchTerm
      }
    }).valueChanges
      .pipe(
        map(result => result.data.allCompanies)
      )
  }

  upvoteCompany(id: String) {
    return this.apollo.mutate({
      mutation: gql `
       mutation upvote ($id: String!) {
        upvote(id: $id) {
          id
          name
          voteCount
        }
       }
      `,
      variables: {
        id: id
      }
    })
  }
  downvoteCompany(id: String) {
    return this.apollo.mutate({
      mutation: gql `
       mutation downvote ($id: String!) {
        downvote(id: $id) {
          id
          name
          voteCount
        }
       }
      `,
      variables: {
        id: id
      }
    })
  }

}
