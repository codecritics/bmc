import {Component, OnInit, Input} from '@angular/core';
import {Company} from '../types'
import {CompanyService} from '../company.service'

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {
  @Input() company: Company

  constructor(private companyservice: CompanyService) {

  }

  ngOnInit() {
  }

  upvote(id: String) {
    this.companyservice.upvoteCompany(id).subscribe(({data}) => {
      console.log('upvoted', data)
    }, (error) => {
      console.log("failed to upvote", error)
    })
  }

  downvote(id: String) {
    this.companyservice.downvoteCompany(id).subscribe(({data}) => {
      console.log('downvoted', data)
    }, (error) => {
      console.log("failed to downvote", error)
    })
  }

}
